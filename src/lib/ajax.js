var https = require("https");

const ajax = options => new Promise ((resolve, reject) => {
    var req = https.request(options, res => {
        var output = '';
        res.setEncoding('utf8');

        res.on('data', function (chunk) {
            output += chunk;
        });

        res.on('end', () => {
            if (res.statusCode !== 200) {
                reject('Failed with response code ' + res.statusCode);
            } else {
                resolve(output);
            }
        });
    });

    if (options.method == 'POST') req.write(options.data);

    req.on('error', reject);

    req.end();
});

module.exports = ajax;