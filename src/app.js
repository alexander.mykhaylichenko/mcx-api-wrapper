const ajax = require('./lib/ajax');

'use strict';

const mcxapi = {
    caseApiFactory: function ({
        server = 'EU',
        language = 'en-GB',
        userName = isRequired('userName'),
        password = isRequired('password'),
        companyName = isRequired('companyName')
    } = {}) {
        return new apiSession({

            // fundamentals
            credentials: {
                userName,
                password,
                companyName
            },
            server: {
                US: {
                    host: 'caseapi.allegiancetech.com',
                    path: '/CaseManagement.svc',
                },
                EU: {
                    host: 'caseapi.mcxplatform.de',
                    path: '/CaseManagement.svc',
                },
                AU: {
                    host: 'caseapi.mcxplatform.com.au',
                    path: '/CaseManagement.svc',
                }
            } [server],
            language,

            // public calls and API-specific modifiers
            calls: {
                getInteractionPointUserList: {
                    endpoint: '/getInteractionPointUserList',
                    responseProperties: {
                        getAllowedUsers: function () {
                            return this.GetUserGetInteractionPointUsersResult.userAccount.filter(u => !u.Hide);
                        },
                        getForbiddenUsers: function () {
                            return this.GetUserGetInteractionPointUsersResult.userAccount.filter(u => u.Hide);
                        }
                    }
                }
            },
            modifiers: {
                request: {
                    correctDates: (options) => ( // formats any date object or valid date string into MCX Case API valid date string
                        Object.keys(options).reduce((acc, k) => {
                            acc[k] = options[k];
                            if (/date/i.test(k)) {
                                acc[k] = (
                                    acc[k] instanceof Date ? acc[k] : (new Date(acc[k]))
                                ).toISOString().replace(/(\d+).(\d+).(\d+).([^A-Z]+)./, '$3-$2-$1 $4')
                            }
                            return acc;
                        }, {})
                    )
                }
            }
        })
    },
    responseApiFactory: function ({
        server = 'EU',
        language = 'en-GB',
        userName = isRequired('userName'),
        password = isRequired('password'),
        companyName = isRequired('companyName')
    } = {}) {
        return new apiSession({

            // fundamentals
            credentials: {
                userName,
                password,
                companyName
            },
            server: {
                US: {
                    host: 'sampleapi.allegiancetech.com',
                    path: '/HttpService.svc/web',
                },
                EU: {
                    host: 'sampleapi.mcxplatform.de',
                    path: '/HttpService.svc/web',
                },
                AU: {
                    host: 'sampleapi.mcxplatform.com.au',
                    path: '/HttpService.svc/web',
                }
            } [server],
            language,

            // public calls and API-specific modifiers
            calls: {
                getSurveyList: {
                    endpoint: '/getSurveyList',
                    responseProperties: {
                        getSurveyIds: function () {
                            return this.GetSurveyListResult.map(s => s.SurveyId);
                        }
                    }
                }
            },
            modifiers: {
                request: {
                    correctDates: (options) => ( // formats any date object or valid date string into MCX Response API valid date string
                        Object.keys(options).reduce((acc, k) => {
                            acc[k] = options[k];
                            if (/date/i.test(k)) {
                                acc[k] = (
                                    acc[k] instanceof Date ? acc[k] : (new Date(acc[k]))
                                ).toISOString().replace(/(\d+).(\d+).(\d+).([^A-Z]+)./, '$3-$2-$1 $4')
                            }
                            return acc;
                        }, {})
                    ),
                    correctFilters: (options) => { // formats JSON formatted filters into XML
                        const dig = (filter, key, topLevel) => { // dig into next layer of object
                            if (filter instanceof Array) return arr(filter, key);
                            if (filter instanceof Object) {
                                Object.keys(filter).forEach(k => { // convert _ values to key parameters
                                    if (filter[k]._) {
                                        let newKey = Object.keys(filter[k]._).reduce(
                                            (acc, pk) => acc += `${k} ${pk}=${JSON.stringify(filter[k]._[pk].toString())}`, '');

                                        filter[newKey] = filter[k];
                                        delete filter[k];
                                        delete filter[newKey]._;
                                    }
                                });
                                return obj(filter, key, topLevel);
                            } else return toXML(filter, key);
                        }
                        const obj = (obj, pk, il) => {
                            let next = Object.keys(obj).reduce((acc, k) => acc += dig(obj[k], k), '');
                            return il ? next : toXML(next, pk);
                        }
                        const arr = (arr, pk) => arr.reduce((acc, v) => acc += dig(v, pk), '');
                        const toXML = (val, key) => `<${key}>${val}</${(key.replace(/[\w]+="[\w]+"/g, '').trim())}>`;

                        return Object.keys(options).reduce((acc, k) => {
                            acc[k] = options[k];
                            if (/filter/i.test(k) && acc[k] instanceof Object) {
                                acc[k] = dig(acc[k], k, true);
                            }
                            return acc;
                        }, {})
                    }
                }
            }
        })
    }
};

module.exports = mcxapi;

// helpers

// builds the basic API Object to return to the user, for both Case and Response
function apiSession({
    credentials = { // login credentials
        userName,
        password,
        companyName
    },
    server = { // API server details
        host,
        path
    },
    language = 'en-GB', // language for call headers
    calls = {}, // explicitly defined calls for endpoints
    modifiers: { // call parameter and response modifiers
        request: requestModifiers = {}, // modify all data sent to the call/request
        response: responseModifiers = {} // modify all response data sent back from call/request
    }
} = {}) {
    // private vars
    let token = ''; // store the authentication token here
    let lastError = '';

    // public vars
    // add headers to server info
    server.headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Accept-Language': language
    };
    // add 'read-only' properties - all non-enumerable
    Object.defineProperties(this, {
        lastError: {
            get() {
                return lastError;
            }
        },
        serverInfo: {
            get() {
                return {
                    ...server,
                    headers: {
                        ...server.headers
                    }
                };
            }
        }
    });

    // boilerplate for all MCX API calls
    // wrapper for xhr - most basic formatting before calling any API endpoint
    const rawPost = (endpoint, data) => (
        ajax({
            path: server.path + endpoint.replace(/(^[^\/])/, "/$1"), // add "/" if missing
            hostname: server.host,
            method: 'POST',
            headers: server.headers,
            data: JSON.stringify(data)
        })
        .then(res => JSON.parse(res))
        //.catch(e => e) // low-level methods will not do error-handling
    );
    // authenticate both Case and Response API
    const authenticate = () => new Promise((resolve, reject) => (
        rawPost('/authenticate', credentials)
        .then(res => {
            if (res.AuthenticateResult) {
                if (res.AuthenticateResult instanceof Object) { // case
                    token = res.AuthenticateResult.token;
                } else { // response
                    token = res.AuthenticateResult;
                }
                resolve(true);
            } else {
                token = '';
                resolve(false);
            }
        })
        .catch(e => reject(`failed to authenticate: ${e}`))
    ));
    // add the most complex endpoint-calling method to the API session object - auto-authenticating call - combination of rawCall and authenticate
    const buildCall = ({
        endpoint = isRequired('endpoint'),
        data = {},
        onProgressUpdate = () => {}
    }) => (
        new Promise(async resolve => {
            const autoAuthCall = (data) => new Promise(resolve => (
                rawPost(endpoint, data)
                .then(response => {
                    lastError = '';
                    resolve(response);
                })
                .catch(async e => {
                    const errorMessageIntro = `call to endpoint ${endpoint} failed: `;
                    
                    if (!e || e.includes('401')) { // try to authenticate on fail
                        try {
                            await authenticate();
                        } catch (e) {
                            reject(errorMessageIntro + (e | 'Unknown error'));
                        }

                        // try again
                        rawPost(endpoint, data)
                            .then(response => {
                                lastError = '';
                                resolve(response);
                            })
                            .catch(e => {
                                lastError = errorMessageIntro + (e | 'Unknown error');
                                resolve(null);
                            });
                    } else {
                        lastError = errorMessageIntro + (e | 'Unknown error');
                        resolve(null);
                    }
                })
            ));

            data = data instanceof Array ? data : [data]; // turn data into array if not already
            for (let mod in requestModifiers) {
                data = data.map(requestModifiers[mod]); // apply data requestModifiers
            }
            data.forEach(d => d.token = token); // add token

            let chunkSize = 4;
            let results = [];
            let chunkResults = [];
            let retry = false;
            let totalChunks = Math.ceil(data.length / chunkSize);

            // make calls in chunks
            for (let i = 0; i < data.length; i += chunkSize) {
                chunkResults = await Promise.all(data.slice(i, i + chunkSize).map(d => autoAuthCall(d)));

                if (!retry && chunkResults.includes(null)) { // at least one of the calls failed. Retry whole chunk
                    onProgressUpdate(i / chunkSize, totalChunks, true);
                    retry = true;
                    i -= chunkSize;
                    await new Promise(resolve => setTimeout(resolve, 10000)); // block for 10 secs to give the MCX API a rest
                } else {
                    retry = false;
                    results = results.concat(chunkResults);
                    onProgressUpdate(i / chunkSize, totalChunks, false);
                }
            }

            Object.keys(responseModifiers).forEach(mod => { // apply response modifiers to each response
                results = results.map(r => r !== null ? responseModifiers[mod](r) : r)
            });

            if (data.length == 1) results = results[0];

            resolve(results);
        })
    );

    // add all explicitly defined endpoint calls
    Object.keys(calls).forEach(call => {
        const {
            endpoint,
            responseProperties = {}
        } = calls[call];

        this[call] = (config = { // defaults
            data: {},
            onProgressUpdate: () => {}
        }) => (
            buildCall({
                endpoint,
                ...config
            })
            .then(res => {
                if (res instanceof Object) { // add special methods/properties to a response object
                    Object.entries(responseProperties).forEach(([prop, val]) => (
                        responseProperties[prop] = {
                            value: val,
                            writable: false,
                            enumerable: false
                        }
                    ));
                    Object.defineProperties(res, responseProperties);
                }
                return res;
            })
        )
    });
    // default method to call any endpoint. Only use this if required endpoint has not been explicitly defined
    this.call = (config = { // defaults
        endpoint = isRequired('endpoint'),
        data = {},
        onProgressUpdate = () => {}
    } = {}) => buildCall(config);

    // finish
    // authenticate on init
    return new Promise((resolve, reject) => (
        authenticate()
        .then(() => resolve(this))
        .catch(e => reject(e))
    ));
}

// enforce mandarory arguments - use only for client-facing functions/methods
const isRequired = arg => {
    throw new Error(`${arg} is a required argument`)
};