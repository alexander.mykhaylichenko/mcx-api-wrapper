# MCX API Wrapper

A wrapper for the MCX API allowing it to be used easily in a node.js app

> **Warning: this is still not ready for production, do not use**

> **Warning: Readme and docs still in-progress**

## Installation

Install with npm:
```bash
npm i git+https://alexander.mykhaylichenko:fcwUXcEWi8gn5u94vukK@gitlab.com/alexander.mykhaylichenko/mcx-api-wrapper
```

## Usage

```javascript
const mcxapi = require('@alexander.mykhaylichenko/mcx-api-wrapper');
```

Firstly, create an API session, a session is tied to one set of credentials for either the response or case API.
Use the **responseApiFactory** to create a Response API session or **caseApiFactory** to create a Case API session, you will need a valid Platform user's **usename**, **password**, and a **company name** as shown in the [MCX API docs](https://developer.maritzcx.com/api/).

The _factory_ functions will throw an error if they are unable to authenticate this user/credentials so if your app was backup credentials to try and you do not wish it to terminate on error then feel free to catch it and try again like in the example below.

```javascript
let responseSession;
try {
    responseSession = await new mcxapi.responseApiFactory({
        userName: '<your username>',
        password: '<your password>',
        companyName: '<the company/client name (domain)>'
    });
} catch (e) {
    console.error(e);
    
    // try something else or some other credentials?
}
```


